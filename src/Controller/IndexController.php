<?php

declare(strict_types=1);

namespace Workshop\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class IndexController extends AbstractController
{
    #[Route('/', name: 'status.get', methods: ['GET'])]
    public function roll(): Response
    {
        return new JsonResponse(['status' => 'OK']);
    }
}
